import Schiff
import Spielfeld


#Simuliert den Zug des Spielers
def zugSpieler(tempschussfeld, tempspielfeldGegner):
    print("Dein Schussfeld:")
    Spielfeld.spielfeld_Ausgabe(tempschussfeld)
    print("Bitte Zielzone angeben")
    zeile = int(input("Zeile:")) - 1
    spalte = int(input("Spalte:")) - 1

    schiffNummer, tempspielfeldGegner = Schiff.schiff_Identifizieren(tempspielfeldGegner, zeile, spalte)
    if schiffNummer != ",":
        tempschussfeld[zeile][spalte] = "X"
        print("Treffer")
    else:
        if tempschussfeld[zeile][spalte] != "X":
            tempschussfeld[zeile][spalte] = "O"
        print("Daneben")
    return schiffNummer, tempschussfeld, tempspielfeldGegner

def SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss): #Schießt, und überprüft ob auf dem beschossenen Feld ein Schiff ist
    schiffnummer, tempspielfeldSpieler = Schiff.schiff_Identifizieren(tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss)
    if schiffnummer != ",":
        tempschussfeld[zeilePositionSchuss][spaltePositionSchuss] = "T"
    else:
        tempschussfeld[zeilePositionSchuss][spaltePositionSchuss] = "x"
    return tempschussfeld, schiffnummer

def zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, befehlSchuss, kleinsteSchiffLänge):
    schiffnummer = ""
    breiteSchussfeld = len(tempschussfeld[0])
    längeSchussfeld = len(tempschussfeld)
    if tempschussfeld[zeilePositionSchuss][spaltePositionSchuss] == ",":
        tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss)
        if schiffnummer != ",":
            return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 1, [zeilePositionSchuss, spaltePositionSchuss]
        else:
            return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 0, [zeilePositionSchuss, spaltePositionSchuss]

    if befehlSchuss == 0:  # Schachbrettmuster ablaufen
        if spaltePositionSchuss + kleinsteSchiffLänge < breiteSchussfeld:
            if tempschussfeld[zeilePositionSchuss][spaltePositionSchuss + kleinsteSchiffLänge] != "X" and tempschussfeld[zeilePositionSchuss][spaltePositionSchuss + kleinsteSchiffLänge] != "T":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss + kleinsteSchiffLänge)
                if schiffnummer != ",":
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss + kleinsteSchiffLänge, 1, [zeilePositionSchuss, spaltePositionSchuss + kleinsteSchiffLänge]
                else:
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss + kleinsteSchiffLänge, 0, [zeilePositionSchuss, spaltePositionSchuss + kleinsteSchiffLänge]
            else:
                print(zeilePositionSchuss)
                print(spaltePositionSchuss)
                print(kleinsteSchiffLänge)
                return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss + kleinsteSchiffLänge, befehlSchuss, kleinsteSchiffLänge)
        else:
            return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss + 1, (zeilePositionSchuss + 1) % kleinsteSchiffLänge, befehlSchuss, kleinsteSchiffLänge)

    if befehlSchuss == 1:  # oben
        if zeilePositionSchuss - 1 >= 0:
            print("HI")
            if tempschussfeld[zeilePositionSchuss - 1][spaltePositionSchuss] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss - 1, spaltePositionSchuss)
                if schiffnummer != ",":
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 5, [zeilePositionSchuss - 1, spaltePositionSchuss]
                else:
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 2, [zeilePositionSchuss - 1, spaltePositionSchuss]
            else:
                return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, 2, kleinsteSchiffLänge)
        else:
            return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, 2, kleinsteSchiffLänge)

    if befehlSchuss == 2:  # links
        if spaltePositionSchuss - 1 >= 0:
            if tempschussfeld[zeilePositionSchuss][spaltePositionSchuss - 1] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss - 1)
                if schiffnummer != ",":
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 6, [zeilePositionSchuss, spaltePositionSchuss - 1]
                else:
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 3, [zeilePositionSchuss, spaltePositionSchuss - 1]
            else:
                return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, 3, kleinsteSchiffLänge)
        else:
            return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, 3, kleinsteSchiffLänge)

    if befehlSchuss == 3:  # rechts
        if spaltePositionSchuss + 1 < breiteSchussfeld:
            if tempschussfeld[zeilePositionSchuss][spaltePositionSchuss + 1] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss + 1)
                if schiffnummer != ",":
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 6, [zeilePositionSchuss, spaltePositionSchuss + 1]
                else:
                    return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 4, [zeilePositionSchuss, spaltePositionSchuss + 1]
            else:
                return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, 4, kleinsteSchiffLänge)
        else:
            return zugComputer(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, spaltePositionSchuss, 4, kleinsteSchiffLänge)

    if befehlSchuss == 4:  # unten
        if tempschussfeld[zeilePositionSchuss + 1][spaltePositionSchuss] == ",":
            tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss + 1, spaltePositionSchuss)
            if schiffnummer != ",":
                return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 5, [zeilePositionSchuss + 1,spaltePositionSchuss]

    if befehlSchuss == 5:  # vertikal
        i = zeilePositionSchuss
        while tempschussfeld[i][spaltePositionSchuss] != "x" and i >= 0:
            if tempschussfeld[i][spaltePositionSchuss] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, i, spaltePositionSchuss)
                return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 5, [i, spaltePositionSchuss]
            i -= 1
        i = zeilePositionSchuss
        while tempschussfeld[i][spaltePositionSchuss] != "x" and i < längeSchussfeld:
            if tempschussfeld[i][spaltePositionSchuss] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, i, spaltePositionSchuss)
                return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 5, [i, spaltePositionSchuss]
            i += 1

    if befehlSchuss == 6:  # horizontal
        i = spaltePositionSchuss
        while tempschussfeld[zeilePositionSchuss][i] != "x" and i >= 0:
            if tempschussfeld[zeilePositionSchuss][i] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, i)
                return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 5, [zeilePositionSchuss, i]
            i -= 1
        i = spaltePositionSchuss
        while tempschussfeld[zeilePositionSchuss][i] != "x" and i < breiteSchussfeld:
            if tempschussfeld[zeilePositionSchuss][i] == ",":
                tempschussfeld, schiffnummer = SCHUSS(tempschussfeld, tempspielfeldSpieler, zeilePositionSchuss, i)
                return tempschussfeld, zeilePositionSchuss, spaltePositionSchuss, 5, [zeilePositionSchuss, i]
            i += 1