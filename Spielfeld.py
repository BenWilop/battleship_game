from copy import deepcopy

#Erzeugt leeres Spielfeld
def spielfeld_Erzeugen(x, y):
    tempSpielfeld = []
    tempZeile = []
    for i in range(y):
        tempZeile += [","]
    for j in range(x):
        tempSpielfeld.append(deepcopy(tempZeile))
    return(tempSpielfeld)

#Gibt Spielfeld in der Konsole aus
def spielfeld_Ausgabe(spielfeld):
    print("")
    for r in spielfeld:
        print(r)
    print("")
