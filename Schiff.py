import random

#Falls Schuss getroffen hat, wird das Schiffteil entfernt und das entsprechende Spielfeld zurückgegeben
def schiff_Identifizieren(tempspielfeld, zeile, spalte):
    schiffNummer = tempspielfeld[zeile][spalte]
    if schiffNummer != ",":
        tempspielfeld[zeile][spalte] = ","
    return schiffNummer, tempspielfeld

#Setzt ein Schiff mit gegebener Länge in ein gegebenes Feld unter Beachtung der gegeben Schiffe
def schiff_Generieren(tempspielfeld, tempspielfeldKontrolle, schiffLänge, schiffNummer):
    spielfeldHöhe = len(tempspielfeld)
    spielfeldBreite = len(tempspielfeld[0])

    r = int(random.uniform(1, 3))
    if schiffLänge > spielfeldBreite:
        r = 1
    if schiffLänge > spielfeldHöhe:
        r = 2
    if r == 1:
        j = 0
        while j != 1:
            x = int(random.uniform(0, spielfeldBreite))
            y = int(random.uniform(0, spielfeldHöhe - (schiffLänge - 1)))
            j = 1
            for i in range(0, schiffLänge):
                if tempspielfeldKontrolle[y + i][x] == 1:
                    j = 0
        for i in range(1, schiffLänge + 1):
            tempspielfeld[y][x] = ""+str(schiffNummer)+""
            tempspielfeldKontrolle[y][x] = 1
            if y - 1 >= 0:
                tempspielfeldKontrolle[y - 1][x] = 1
            if x + 1 < spielfeldBreite:
                tempspielfeldKontrolle[y][x + 1] = 1
            if y + 1 < spielfeldHöhe:
                tempspielfeldKontrolle[y + 1][x] = 1
            if x - 1 >= 0:
                tempspielfeldKontrolle[y][x - 1] = 1
            y += 1

    if r == 2:
        j = 0
        while j != 1:
            x = int(random.uniform(0, spielfeldBreite - (schiffLänge - 1)))
            y = int(random.uniform(0, spielfeldHöhe))
            j = 1
            for i in range(0, schiffLänge):
                if tempspielfeldKontrolle[y][x + i] == 1:
                    j = 0
        for i in range(1, schiffLänge + 1):
            tempspielfeld[y][x] = ""+str(schiffNummer)+""
            tempspielfeldKontrolle[y][x] = 1
            if y - 1 >= 0:
                tempspielfeldKontrolle[y - 1][x] = 1
            if x + 1 < spielfeldBreite:
                tempspielfeldKontrolle[y][x + 1] = 1
            if y + 1 < spielfeldHöhe:
                tempspielfeldKontrolle[y + 1][x] = 1
            if x - 1 >= 0:
                tempspielfeldKontrolle[y][x - 1] = 1
            x += 1

    return tempspielfeld, tempspielfeldKontrolle
