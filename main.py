from copy import deepcopy
import Schiff
import Spielfeld
import Runde

spielfeld = []
gegnerSchiffe = []
spielstopp = False
schiffe = []
fertig = False

print("Bitte geben Sie im Folgenden die gewünschte Spielfeldgröße an")
spielfeldZeilenanzahl = int(input("Zeilenanzahl:"))
spielfeldSpaltenanzahl = int(input("Spaltenanzahl:"))
spielfeld = Spielfeld.spielfeld_Erzeugen(spielfeldZeilenanzahl, spielfeldSpaltenanzahl)

spielfeldGegnerKontrolle = deepcopy(spielfeld)
spielfeldGegner = deepcopy(spielfeld)
schussfeld = deepcopy(spielfeld)
spielfeldSpieler = deepcopy(spielfeld)
spielfeldSpielerKontrolle = deepcopy(spielfeld)
schussfeldKI = deepcopy(spielfeld)
spielfeldAnzeige = deepcopy(spielfeld)



print("Benutzte Schiffe eingeben:")
while fertig == False:
    schiffe += [int(input("Bitte Schifflänge eingeben:"))]
    if input("Fertig? Ja/Nein") == "Ja":
        fertig = True

for i in range(0, len(schiffe)):
    spielfeldGegner, spielfeldGegnerKontrolle = Schiff.schiff_Generieren(spielfeldGegner, spielfeldGegnerKontrolle, schiffe[i], i)
    gegnerSchiffe.append(schiffe[i])

print("Du hast folgende Schiffe zur Verfügung:")
for i in range(0, len(schiffe)):
    print("")
    print("Länge: " + str(schiffe[i]))
print("")
for i in range(0, len(schiffe)):
    a = 0
    while a != 1:
        print("Bitte ein Schiff der Länge " + str(schiffe[i]) + " platzieren")
        y = int(input("Zeile?")) - 1
        x = int(input("Spalte?")) - 1
        r = input("Richtung? Rechts/Unten")
        if r == "Unten":
            if y + schiffe[i] > len(spielfeldSpieler):
                print("Neu setzen")
            else:
                b = 0
                for j in range(0, schiffe[i]):
                    if spielfeldSpielerKontrolle[j + y][x] != ",":
                        b = 1
                if b == 0:
                    for j in range(0, schiffe[i]):
                        spielfeldSpieler[j + y][x] = "" + str(i) + ""
                        spielfeldSpielerKontrolle[j + y][x] = 1
                        if j + y - 1 >= 0:
                            spielfeldSpielerKontrolle[j + y - 1][x] = 1
                        if j+ y + 1 < len(spielfeldSpielerKontrolle):
                            spielfeldSpielerKontrolle[j + y + 1][x] = 1
                        if x - 1 >= 0:
                            spielfeldSpielerKontrolle[j + y][x - 1] = 1
                        if x + 1 < len(spielfeldSpielerKontrolle[0]):
                            spielfeldSpielerKontrolle[j + y][x + 1] = 1
                    a = 1
                else:
                    print("Neu setzen")
        elif r == "Rechts":
            if x + schiffe[i] - 1 > len(spielfeldSpieler[0]):
                print("Neu setzen")
            else:
                b = 0
                for j in range(0, schiffe[i]):
                    if spielfeldSpielerKontrolle[y][x] != ",":
                        b = 1
                if b == 0:
                    for j in range(0, schiffe[i]):
                        spielfeldSpieler[y][j + x] = "" + str(i) + ""
                        spielfeldSpielerKontrolle[y][j + x] = 1
                        if y - 1 >= 0:
                            spielfeldSpielerKontrolle[y - 1][j + x] = 1
                        if y + 1 < len(spielfeldSpielerKontrolle):
                            spielfeldSpielerKontrolle[y + 1][j + x] = 1
                        if j + x - 1 >= 0:
                            spielfeldSpielerKontrolle[y][j + x - 1] = 1
                        if j + x + 1 < len(spielfeldSpielerKontrolle[0]):
                            spielfeldSpielerKontrolle[y][j + x + 1] = 1
                    a = 1
                else:
                    print("Neu setzen")
        else:
            print("Neu setzen")

zug = 1
zeile = 0
spalte = 0
befehl = 0
while spielstopp == False:
    if zug == 1:
        schiffNummer, schussfeld, spielfeldGegner = Runde.zugSpieler(schussfeld, spielfeldGegner)
        if schiffNummer != ",":
            gegnerSchiffe[int(schiffNummer)] -= 1
            if gegnerSchiffe[int(schiffNummer)] == 0:
                print("Schiff zerstört")
        spielstopp = True
        for i in range(0, len(gegnerSchiffe)):
            if gegnerSchiffe[i] != 0:
                spielstopp = False
        if spielstopp == True:
            print("Du hast gewonnen")
        zug = 2
    if zug == 2:
        z = 0
        for i in range(0, len(schiffe)):
            if z == 0:
                if schiffe[i] != 0:
                    temp = schiffe[i]
                    z = 1
            if schiffe[i] < temp:
                if schiffe[i] != 0:
                    temp = schiffe[i]
        kleinsteSchiffLänge = temp
        tempFeld = deepcopy(spielfeldSpieler)
        schussfeldKI, zeile, spalte, befehl, feld = Runde.zugComputer(schussfeldKI, spielfeldSpieler, zeile, spalte, befehl, kleinsteSchiffLänge)
        nummer = deepcopy(tempFeld[feld[0]][feld[1]])
        spielfeldSpieler[feld[0]][feld[1]] = ","
        print(nummer)
        if nummer != ",":
            schiffe[int(nummer)] -= 1
            if schiffe[int(nummer)] == 0:
                print("Dein Schiff wurde zerstört")
                befehl = 0
                for i in range(0, len(schussfeldKI)):
                    for j in range(0, len(schussfeldKI[0])):
                        if schussfeldKI[i][j] == "T":
                            if i - 1 >= 0 and schussfeldKI[i - 1][j] != "T":
                                schussfeldKI[i - 1][j] = "X"
                            if i + 1 < len(schussfeldKI) and schussfeldKI[i + 1][j] != "T":
                                schussfeldKI[i + 1][j] = "X"
                            if j - 1 >= 0 and schussfeldKI[i][j - 1] != "T":
                                schussfeldKI[i][j - 1] = "X"
                            if j + 1 < len(schussfeldKI[0]) and schussfeldKI[i][j + 1] != "T":
                                schussfeldKI[i][j + 1] = "X"
        spielstopp = True
        for i in range(0, len(schiffe)):
            if schiffe[i] != 0:
                spielstopp = False
        if spielstopp == True:
            print("Du hast verloren")
        zug = 1
        print("Deine Schiffe")
        spielfeldAnzeige = deepcopy(spielfeldSpieler)
        for i in range(0,len(spielfeldAnzeige)):
            for j in range(0, len(spielfeldAnzeige[0])):
                if spielfeldAnzeige[i][j] == ",":
                    spielfeldAnzeige[i][j] = schussfeldKI[i][j]
        Spielfeld.spielfeld_Ausgabe(spielfeldAnzeige)